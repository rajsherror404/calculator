const path = require('path')
const dotEnvPath = path.resolve('.env')
require('dotenv').config({ path: dotEnvPath })

module.exports = {
  'url': process.env.DATABASE_URL
}
