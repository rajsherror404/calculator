let app = require('../../../../app/calculator')
let logger = require('../../../../infrastructure/logger')
let validator = require('../../validators')

exports.execute = (req) => {
    logger.info("Evaluating packet : " + JSON.stringify(req.body))
    try{
        logger.debug("Evaluating expression: " + req.body.expression)
        validator.validate(req);
        logger.debug("Evaluating expression: " + req.body.expression)
        const result = app.execute(req.body.expression);
        return result;
    }
    catch(error){
        logger.error("Error in execution.: " + error)
        throw Error(error)
    }
} 
 