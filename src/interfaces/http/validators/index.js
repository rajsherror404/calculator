const payloadChecker = require('payload-validator')
const logger = require('../../../infrastructure/logger')
const CircularJSON = require('circular-json')
var expectedPayload = {
    "expression" : ""
};


exports.validate = (req) => {
    logger.info("Validating packet")
    validateMandatoryParams(req);
    if(!req.body) {
        throw Error("paylod not correct");
    } 
    var result = payloadChecker.validator(req.body,expectedPayload,["expression"],true);
    logger.debug(CircularJSON.stringify(result));
    if(result.success) {
        logger.debug("Packet validated")
        return;
    } else {
        throw Error(result.response.errorMessage);
    }
}

const validateMandatoryParams = (req) => {
    logger.debug("Validating mandatory parameters")
    if(!req.body.expression){
        throw Error("Mandatory parameters missing");
    }
}