let uuid = require('uuid/v4')
let cls = require('cls-hooked');
cls.createNamespace('context');

module.exports = ((req, res, next) => {
  var nameSpace = cls.getNamespace('context');

  // run middleware in the scope of the namespace that we have created
  nameSpace.run(() => {
      // set data to the namespace that we want to access in different events/callbacks
      nameSpace.set('traceId', uuid());
      let data = nameSpace.get("traceId");
      console.log(data);
      next();
  });
});

