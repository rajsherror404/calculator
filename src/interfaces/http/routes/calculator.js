const express = require('express')
const router = express.Router();
const app = require('../modules/expression')
const logger = require('../../../infrastructure/logger')

router.get('/hello', (req,res)=> {
    res.status(200).send("Hello! How are you");
})

router.post('/expression', (req,res)=> {
    if(!req.body){
        return res.status(400).send("Request body is missing.")
    }
    try{
        const result = app.execute(req);
        return res.status(200).send({
            result : result
        });
    }
    catch(error){
        logger.error(error)
        return res.status(400).send({
            "message" : error.message
        })
    }
})

router.post('/test', (req,res)=> {
    if(!req.body){
        return res.status(400).send("Request body is missing.")
    }
    res.status(200).send("Hello! How are you");
})

router.get('/error', (req,res) => {
    throw new Error("This is a forced error!")
})
module.exports = router;