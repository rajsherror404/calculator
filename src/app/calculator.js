var repository = require('../infrastructure/repositories/expression')
var logger = require('../infrastructure/logger')

exports.execute = (expression) =>{
    try{
        logger.info("Evaluating expression : " + expression)
        let result = eval(expression);
        logger.info("Evaluated with result : " + result)
        repository.create(expression,result)
        return result
    }
    catch(error){
        logger.error(error)
        throw Error("Invalid Expression : " + expression)
    }
}