var mongoose = require('mongoose')

const expressionSchema = new mongoose.Schema({
    _expression : {type : String, required : true},
    _result : {type : Number},
}, {
    timestamps: true
});

module.exports = mongoose.model('expression', expressionSchema)