let mongoose = require('mongoose')
let dbConfig = require('../../../config/database')
let logger = require('../logger')


exports.authenticate = () =>{
    // Connecting to the database
    mongoose.connect(dbConfig.url, {
        useNewUrlParser: true
    }).then(() => {
        logger.info("Successfully connected to the database");    
    }).catch(err => {
        logger.info('Could not connect to the database. Exiting now...' + err);
        process.exit();
    });

    // Configuring the database
    mongoose.Promise = global.Promise;
}