let expression = require('../database/models/expression')
let logger = require('../logger')

exports.create = (query, result) => {
    logger.debug("Attempt to create an expression entry.")
     const db = new expression({
         _expression : query,
         _result : result
     });

     db.save()
     .then(data => {
        logger.debug("saved successfully entry to db " + data)
     })
     .catch(err => {
        logger.error(err);
        throw new Error(err)
     })
}