const path = require('path')
const moment = require('moment-timezone')
const cls = require('cls-hooked');

const { createLogger, format, transports } = require('winston');
const { combine, label, timestamp, printf } = format;
const logFormat = printf((info) =>
  {
    var nameSpace = cls.getNamespace('context');
    if(nameSpace){
      data = nameSpace.get("traceId");
    }
    return data 
    ?  `${info.timestamp} ${data} [${info.level}]: ${info.label} - ${info.message}`
    :   `${info.timestamp}  [${info.level}]: ${info.label} - ${info.message}`
  }
  );
const appendTimestamp = format((info, opts) => {
  if(opts.tz)
    info.timestamp = moment().tz(opts.tz).format();
  return info;
});

const logger = createLogger({
  level: process.env.LOG_LEVEL || 'debug',
  format: combine(
    format.label({ label: path.basename(process.mainModule.filename) }),
    appendTimestamp({ tz: process.env.TIMEZONE }),
    timestamp(),
    logFormat
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.Console(),
    new transports.File({ filename: 'error.log', level: 'error' }),
    new transports.File({ filename: 'combined.log' })
  ]
})
  
module.exports = logger;