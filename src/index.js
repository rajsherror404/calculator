let express = require('express')
let path = require('path')
let bodyParser = require('body-parser')
let db = require('../src/infrastructure/database/authenticate')
let calculatorRoute = require('./interfaces/http/routes/calculator')
let logger = require('../src/infrastructure/logger')
let uuid = require('uuid/v4')
let CircularJSON = require('circular-json')
require('dotenv').config()

let app = express()

app.use(bodyParser.json())
let cls = require('cls-hooked');
cls.createNamespace('context');

app.use((req, res, next) => {
    var nameSpace = cls.getNamespace('context');
    // wrap the events from request and response
    nameSpace.bindEmitter(req);
    nameSpace.bindEmitter(res);
    // run middleware in the scope of the namespace that we have created
    nameSpace.run(() => {
        // set data to the namespace that we want to access in different events/callbacks
        nameSpace.set('traceId', uuid());
        next();
    });
 });

app.use((req,res, next) => {
    logger.info(`${new Date().toString()} => ${req.originalUrl} ${CircularJSON.stringify(req)}`)
    next()
})

app.use(calculatorRoute);
app.use(express.static('public'))

// Handler for 404 - resource not found
app.use((req,res,next) => {
    res.status(400).send('we think you are lost')
})

// Handler for Error 500
app.use((err, req, res, next) => {
    logger.info(err.stack)
    res.sendFile(path.join(__dirname, '/../public/500.html'))
})

db.authenticate();

const PORT = process.env.PORT;
app.listen(PORT, () => logger.info(`Example app listening on port ${PORT}!`))